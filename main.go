package main

import (
	"gitee.com/peanut_im/routers"
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	store := cookie.NewStore([]byte("secret"))
	r.Use(sessions.Sessions("sessionid", store))
	api := r.Group("/api")
	{
		usr := api.Group("/user")
		{
			usr.POST("/register", routers.Register)
			usr.POST("/login", routers.Login)
			usr.GET("/connect", routers.ConnectWeb)
			usr.GET("/id/:id", routers.GetUserMessageByID)
			usr.Use(routers.Authorize())
			usr.GET("/self", routers.GetSelfMessage)
			usr.PUT("/message", routers.ChangeMessage)
			usr.GET("/friends", routers.GetFriends)
			usr.POST("/find", routers.FindUserByEmail)
			usr.POST("/isfriend", routers.IsFriend)
			usr.POST("/addfriend", routers.AddFriend)
			usr.POST("/deletefriend", routers.DeleteFriend)
			usr.POST("/fid", routers.GetFidByEmail)
		}

		moment := api.Group("/moment")
		{
			moment.Use(routers.Authorize())
			moment.POST("/publish", routers.PublishMoment)
			moment.GET("/all", routers.GetMoments)
			moment.POST("/comment", routers.AddComment)
			moment.GET("/all/:fid", routers.GetUserMoments)
			moment.POST("/like", routers.AddLike)
		}

		group := api.Group("/group")
		{
			group.Use(routers.Authorize())
			group.POST("/create", routers.CreatGroup)
			group.GET("/add/:number", routers.AddGroup)
			group.POST("/announcement", routers.PublishAnnouncement)
			group.POST("/delete", routers.DeleteMemberById)
			group.GET("/all", routers.GetAllGroup)
			group.GET("/id/:number", routers.GetGroup)
			group.DELETE("/delete/:groupid", routers.DeleteGroup)
		}

		chat := api.Group("/chat")
		{
			chat.Use(routers.Authorize())
			chat.GET("/unreadmessages", routers.GetMessages)
			chat.GET("/ping", routers.Pong)
			// chat.POST("/group", routers.WsHandle)
		}
	}
	r.Run(":8899")
}
