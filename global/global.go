package global

const DB = "peanut_im"

const (
	StatusSuccess  = "success"
	StatusFail	   = "fail"
	StatusBadReq   = "bad_req"
	StatusNotLogin = "not_login"
	StatusNotAllow = "not_allow"
	StatusExist    = "had_exist"
	StatusError    = "error"
	StatusInvalid = "invalid"
	StatusNotFound = "not_found"
)
