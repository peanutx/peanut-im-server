package routers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"gitee.com/peanut_im/global"
	"gitee.com/peanut_im/models"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func CreatGroup(c *gin.Context) {
	session := sessions.Default(c)
	v := session.Get("sessionid")
	var user models.User
	models.FindOne(global.DB, "user", bson.M{"_id": bson.ObjectIdHex(v.(string))}, nil, &user)
	var groupreq models.GroupReq
	var group models.Group
	var groupexist models.Group
	json.NewDecoder(c.Request.Body).Decode(&groupreq)
	group.Gid = bson.NewObjectId()

	//groupnum := rand.Intn(1000000)
	sess, err := mgo.Dial("127.0.0.1") //连接数据库
	if err != nil {
		panic(err)
	}
	defer sess.Close()
	sess.SetMode(mgo.Monotonic, true)
	collection := sess.DB("peanut_im").C("group") //数据库名称
	groupnum, _ := collection.Count()

	for {
		models.FindOne(global.DB, "group", bson.M{"number": strconv.Itoa(groupnum)}, nil, &groupexist)
		if groupexist.Gid == "" {
			group.Number = strconv.Itoa(groupnum)
			break
		} else {
			groupnum++
		}
	}
	group.Name = groupreq.Name
	group.Ownerid = v.(string)
	group.Members = append(group.Members, v.(string))
	group.Membersname = append(group.Membersname, user.Info.Name)
	group.Announcement = ""
	models.Insert(global.DB, "group", group)
	//models.Update(db, "group", bson.M{"number": c.Param("number")}, bson.M{"$push": bson.M{"members": v.(string)}})
	models.Update(global.DB, "user", bson.M{"_id": bson.ObjectIdHex(v.(string))}, bson.M{"$push": bson.M{"group": group.Number}})
	c.JSON(http.StatusOK, &Response{
		Code:    200,
		Type:    global.StatusSuccess,
		Message: "",
	})
}

func AddGroup(c *gin.Context) {
	session := sessions.Default(c)
	v := session.Get("sessionid")
	var group models.Group
	models.FindOne(global.DB, "group", bson.M{"number": c.Param("number")}, nil, &group)
	if group.Gid == "" {
		c.JSON(http.StatusOK, &Response{
			Code:    400,
			Type:    global.StatusFail,
			Message: "group not exist",
		})
		return
	}

	var user models.User
	models.FindOne(global.DB, "user", bson.M{"_id": bson.ObjectIdHex(v.(string))}, nil, &user)
	for _, value := range user.Group {
		if c.Param("number") == value {
			c.JSON(http.StatusOK, &Response{
				Code:    400,
				Type:    global.StatusFail,
				Message: "you have joined this group",
			})
			return
		}
	}

	models.Update(global.DB, "group", bson.M{"number": c.Param("number")}, bson.M{"$push": bson.M{"members": v.(string)}})
	models.Update(global.DB, "group", bson.M{"number": c.Param("number")}, bson.M{"$push": bson.M{"membersname": user.Info.Name}})
	models.Update(global.DB, "user", bson.M{"_id": bson.ObjectIdHex(v.(string))}, bson.M{"$push": bson.M{"group": c.Param("number")}})
	c.JSON(http.StatusOK, &Response{
		Code:    200,
		Type:    global.StatusSuccess,
		Message: "",
	})
}

func PublishAnnouncement(c *gin.Context) {
	session := sessions.Default(c)
	v := session.Get("sessionid")
	var groupreq models.AnnouncementReq
	json.NewDecoder(c.Request.Body).Decode(&groupreq)
	var group models.Group
	models.FindOne(global.DB, "group", bson.M{"number": groupreq.Number}, nil, &group)
	if group.Ownerid != v.(string) {
		c.JSON(http.StatusOK, &Response{
			Code:    400,
			Type:    global.StatusFail,
			Message: "only owner can publish announcement",
		})
		return
	}
	models.Update(global.DB, "group", bson.M{"number": groupreq.Number}, bson.M{"$set": bson.M{"announcement": groupreq.Announcement}})
	c.JSON(http.StatusOK, &Response{
		Code:    200,
		Type:    global.StatusSuccess,
		Message: "",
	})
}

func DeleteMemberById(c *gin.Context) {
	session := sessions.Default(c)
	v := session.Get("sessionid")
	var groupreq models.DeleteReq
	var group models.Group
	json.NewDecoder(c.Request.Body).Decode(&groupreq)
	models.FindOne(global.DB, "group", bson.M{"number": groupreq.Number}, nil, &group)
	if group.Ownerid != v.(string) {
		c.JSON(http.StatusOK, &Response{
			Code:    400,
			Type:    global.StatusFail,
			Message: "only owner can delete member",
		})
		return
	}
	models.Update(global.DB, "user", bson.M{"_id": bson.ObjectIdHex(groupreq.MemberId)}, bson.M{"$pull": bson.M{"group": groupreq.Number}})
	models.Update(global.DB, "group", bson.M{"number": groupreq.Number}, bson.M{"$pull": bson.M{"members": groupreq.MemberId}})
	var member models.User
	models.FindOne(global.DB, "user", bson.M{"_id": bson.ObjectIdHex(groupreq.MemberId)}, nil, &member)
	models.Update(global.DB, "group", bson.M{"number": groupreq.Number}, bson.M{"$pull": bson.M{"membersname": member.Info.Name}})
	c.JSON(http.StatusOK, &Response{
		Code:    200,
		Type:    global.StatusSuccess,
		Message: "",
	})
}

func DeleteGroup(c *gin.Context) {
	session := sessions.Default(c)
	v := session.Get("sessionid")
	var group models.Group
	models.FindOne(global.DB, "group", bson.M{"number": c.Param("groupid")}, nil, &group)
	if group.Ownerid != v.(string) {
		c.JSON(http.StatusOK, &Response{
			Code:    400,
			Type:    global.StatusFail,
			Message: "only owner can delete group",
		})
		return
	}
	for _, value := range group.Members {
		models.Update(global.DB, "user", bson.M{"_id": bson.ObjectIdHex(value)}, bson.M{"$pull": bson.M{"group": c.Param("groupid")}})
	}
	models.Remove(global.DB, "group", bson.M{"number": c.Param("groupid")})
	c.JSON(http.StatusOK, &Response{
		Code:    200,
		Type:    global.StatusSuccess,
		Message: "",
	})
}

func GetAllGroup(c *gin.Context) {
	session := sessions.Default(c)
	v := session.Get("sessionid")
	var groupres []models.Group
	var group models.Group
	var u models.User
	models.FindOne(global.DB, "user", bson.M{"_id": bson.ObjectIdHex(v.(string))}, nil, &u)
	for _, v := range u.Group {
		models.FindOne(global.DB, "group", bson.M{"number": v}, nil, &group)
		groupres = append(groupres, group)
	}
	c.JSON(http.StatusOK, &Response{
		Code:    200,
		Type:    global.StatusSuccess,
		Message: groupres,
	})

}

func GetGroup(c *gin.Context) {

	var group models.Group
	models.FindOne(global.DB, "group", bson.M{"number": c.Param("number")}, nil, &group)

	if group.Number == "" {
		c.JSON(http.StatusOK, &Response{
			Code:    400,
			Type:    global.StatusFail,
			Message: "group not exist",
		})
		return
	}

	c.JSON(http.StatusOK, &Response{
		Code:    200,
		Type:    global.StatusSuccess,
		Message: group,
	})

}
