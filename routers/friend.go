package routers

import (
	"encoding/json"
	"net/http"

	"gitee.com/peanut_im/global"
	"gitee.com/peanut_im/models"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

type FriendsRes struct {
	Code  int
	State string
	Data  models.Friend
}

func GetFriends(c *gin.Context) {
	session := sessions.Default(c)
	v := session.Get("sessionid")
	var friends models.Friend
	models.FindOne(global.DB, "friend", bson.M{"_id": bson.ObjectIdHex(v.(string))}, nil, &friends)
	c.JSON(http.StatusOK, &Response{
		Code:    http.StatusOK,
		Type:    global.StatusSuccess,
		Message: friends,
	})
}

func AddFriend(c *gin.Context) {
	session := sessions.Default(c)
	v := session.Get("sessionid")
	var friendreq models.AddFriendReq
	var friend models.User
	var f models.Friend
	models.FindOne(global.DB, "friend", bson.M{"_id": bson.ObjectIdHex(v.(string))}, nil, &f)

	json.NewDecoder(c.Request.Body).Decode(&friendreq)
	models.FindOne(global.DB, "user", bson.M{"email": friendreq.Email}, nil, &friend)

	models.Update(global.DB, "friend", bson.M{"_id": bson.ObjectIdHex(v.(string))}, bson.M{"$push": bson.M{"fid": friend.ID}})
	models.Update(global.DB, "friend", bson.M{"_id": friend.ID}, bson.M{"$push": bson.M{"fid": bson.ObjectIdHex(v.(string))}})
	models.UpdateAll(global.DB, "moment", bson.M{"publisherId": v.(string)}, bson.M{"$push": bson.M{"friends": friend.ID.Hex()}})
	models.UpdateAll(global.DB, "moment", bson.M{"publisherId": friend.ID.Hex()}, bson.M{"$push": bson.M{"friends": v.(string)}})
	c.JSON(http.StatusOK, &Response{
		Code:    200,
		Type:    global.StatusSuccess,
		Message: "add friend success",
	})
}

func IsFriend(c *gin.Context) {
	session := sessions.Default(c)
	v := session.Get("sessionid")
	var friendreq models.AddFriendReq
	var friend models.User
	var f models.Friend
	models.FindOne(global.DB, "friend", bson.M{"_id": bson.ObjectIdHex(v.(string))}, nil, &f)

	json.NewDecoder(c.Request.Body).Decode(&friendreq)
	models.FindOne(global.DB, "user", bson.M{"email": friendreq.Email}, nil, &friend)
	for _, v := range f.Friends {
		if v.Hex() == friend.ID.Hex() {
			c.JSON(http.StatusOK, &Response{
				Code:    400,
				Type:    global.StatusFail,
				Message: "friend exist",
			})
			return
		}
	}
	c.JSON(http.StatusOK, &Response{
		Code: 200,
		Type: global.StatusSuccess,
		Message: "available",
	})
}

func DeleteFriend(c *gin.Context) {
	session := sessions.Default(c)
	v := session.Get("sessionid")
	var friendreq models.AddFriendReq
	var friend models.User

	json.NewDecoder(c.Request.Body).Decode(&friendreq)
	models.FindOne(global.DB, "user", bson.M{"email": friendreq.Email}, nil, &friend)

	models.Update(global.DB, "friend", bson.M{"_id": bson.ObjectIdHex(v.(string))}, bson.M{"$pull": bson.M{"fid": friend.ID}})
	models.Update(global.DB, "friend", bson.M{"_id": friend.ID}, bson.M{"$pull": bson.M{"fid": bson.ObjectIdHex(v.(string))}})
	models.UpdateAll(global.DB, "moment", bson.M{"publisherId": v.(string)}, bson.M{"$push": bson.M{"friends": friend.ID.Hex()}})
	models.UpdateAll(global.DB, "moment", bson.M{"publisherId": friend.ID.Hex()}, bson.M{"$push": bson.M{"friends": v.(string)}})
	c.JSON(http.StatusOK, &Response{
		Code:    200,
		Type:    global.StatusSuccess,
		Message: "delete friend success",
	})
}
