package routers

import (
	"encoding/json"
	"net/http"
	"strings"

	"fmt"

	"gitee.com/peanut_im/global"
	"gitee.com/peanut_im/models"
	"gitee.com/peanut_im/utils"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

type Response struct {
	Code    int         `bson:"code" json:"code"`
	Type    string      `bson:"type" json:"type"`
	Message interface{} `bson:"message" json:"message"`
}

/* POST /api/user/register */
func Register(c *gin.Context) {

	var user models.RegisterReq
	err := json.NewDecoder(c.Request.Body).Decode(&user)
	if err != nil || user.Email == "" || user.Password == "" {
		c.JSON(http.StatusOK, &Response{
			Code:    400,
			Type:    global.StatusFail,
			Message: "username and password do not match",
		})
		return
	}
	err = models.Register(user.Name, user.Email, user.Password)
	if err != nil {
		c.JSON(http.StatusOK, &Response{
			Code:    400,
			Type:    global.StatusFail,
			Message: "username and password do not match",
		})
		return
	}
	c.JSON(http.StatusOK, &Response{
		Code: 200,
		Type: global.StatusSuccess,
		Message: "",
	})
}

/* POST /api/user/login */
func Login(c *gin.Context) {
	session := sessions.Default(c)
	option := sessions.Options{MaxAge: 3600 * 24 * 60, Path: "/"}
	session.Options(option)
	var user models.LoginReq
	err := json.NewDecoder(c.Request.Body).Decode(&user)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusOK, &Response{
			Code:    400,
			Type:    global.StatusFail,
			Message: "username and password do not match",
		})
	}
	exist := models.IsExist(global.DB, "user", bson.M{"email": user.Email})
	if exist {
		err = models.Login(user.Email, user.Password)
		if err != nil {
			c.JSON(http.StatusOK, &Response{
				Code:    400,
				Type:    global.StatusFail,
				Message: "username and password do not match",
			})
		} else {
			var u models.User
			models.FindOne(global.DB, "user", bson.M{"email": user.Email}, nil, &u)
			fmt.Println(u.ID.Hex())
			session.Set("sessionid", u.ID.Hex())
			session.Save()
			///////////////

			///////////////
			c.JSON(http.StatusOK, &Response{
				Code:    200,
				Type:    global.StatusSuccess,
				Message: u.ID.Hex(),
			})
		}

	} else {
		c.JSON(http.StatusOK, &Response{
			Code:    400,
			Type:    global.StatusFail,
			Message: "user not exist",
		})
	}
}

func Authorize() gin.HandlerFunc {
	return func(c *gin.Context) {
		session := sessions.Default(c)
		v := session.Get("sessionid")
		fmt.Println("cookie: ", v)
		if v != nil {
			// 验证通过，会继续访问下一个中间件
			c.Next()
		} else {
			// 验证不通过，不再调用后续的函数处理
			c.Abort()
			c.JSON(http.StatusUnauthorized, gin.H{"message": "访问未授权"})
			return
		}
	}
}

/* GET /api/user/self */
func GetSelfMessage(c *gin.Context) {
	session := sessions.Default(c)
	v := session.Get("sessionid")

	var u models.User
	models.FindOne(global.DB, "user", bson.M{"_id": bson.ObjectIdHex(v.(string))}, nil, &u)
	c.JSON(http.StatusOK, &Response{
		Code:    http.StatusOK,
		Type:    global.StatusSuccess,
		Message: u,
	})
}

/* PUT /api/user/message */
func ChangeMessage(c *gin.Context) {
	session := sessions.Default(c)
	v := session.Get("sessionid")
	var user models.User
	json.NewDecoder(c.Request.Body).Decode(&user)

	var u models.User
	models.FindOne(global.DB, "user", bson.M{"_id": bson.ObjectIdHex(v.(string))}, nil, &u)
	if user.Info.Name != "" {
		u.Info.Name = user.Info.Name
	}
	if user.Email != "" {
		u.Email = user.Email
	}
	if user.Password != "" {
		var data []byte = []byte(user.Password)
		hashCode := utils.GetSHA256HashCode(data)
		u.Password = hashCode
	}
	if user.Info.Bio != "" {
		u.Info.Bio = user.Info.Bio
	}
	if user.Info.Avatar != "" {
		user.Info.Avatar = strings.Replace(user.Info.Avatar, " ", "+", -1)
		u.Info.Avatar = user.Info.Avatar
	}
	if user.Info.Gender == 1 {
		u.Info.Gender = 1
	}
	if user.Info.Gender == 0 {
		u.Info.Gender = 0
	}

	models.Update(global.DB, "user", bson.M{"_id": bson.ObjectIdHex(v.(string))}, bson.M{"$set": bson.M{
		"email":    u.Email,
		"password": u.Password,
		"info":     u.Info,
	}})
	c.JSON(http.StatusOK, &Response{
		Code:    200,
		Type:    global.StatusSuccess,
		Message: "",
	})
}

/* GET /api/user/id/:id */
func GetUserMessageByID(c *gin.Context) {
	var u models.User
	var finfo models.FriendInfo
	models.FindOne(global.DB, "user", bson.M{"_id": bson.ObjectIdHex(c.Param("id"))}, nil, &u)
	finfo.Avatar = u.Info.Avatar
	finfo.Bio = u.Info.Bio
	finfo.Email = u.Email
	finfo.Gender = u.Info.Gender
	finfo.Name = u.Info.Name
	c.JSON(http.StatusOK, &Response{
		Code:    200,
		Type:    global.StatusSuccess,
		Message: finfo,
	})
}

func FindUserByEmail(c *gin.Context) {
	var addfriReq models.AddFriendReq
	var u models.User
	var fInfo models.FriendInfo
	json.NewDecoder(c.Request.Body).Decode(&addfriReq)

	err := models.FindOne(global.DB, "user", bson.M{"email": addfriReq.Email}, nil, &u)
	if err != nil {
		c.JSON(http.StatusOK, &Response{
			Code: 400,
			Type: global.StatusNotFound,
			Message: fInfo,
		})
		return
	}
	fInfo.Email = u.Email
	fInfo.Avatar = u.Info.Avatar
	fInfo.Name = u.Info.Name
	fInfo.Bio = u.Info.Bio
	fInfo.Gender = u.Info.Gender
	c.JSON(http.StatusOK, &Response{
		Code: 200,
		Type: global.StatusSuccess,
		Message: fInfo,
	})
}

func GetFidByEmail(c *gin.Context) {
	var addfriReq models.AddFriendReq
	var u models.User
	json.NewDecoder(c.Request.Body).Decode(&addfriReq)

	err := models.FindOne(global.DB, "user", bson.M{"email": addfriReq.Email}, nil, &u)
	if err != nil {
		c.JSON(http.StatusOK, &Response{
			Code: 400,
			Type: global.StatusError,
			Message: "",
		})
	} else {
		c.JSON(http.StatusOK, &Response{
			Code: 200,
			Type: global.StatusSuccess,
			Message: u.ID.Hex(),
		})
	}
}

