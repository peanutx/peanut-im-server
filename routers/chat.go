package routers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"sync"
	"time"

	"gopkg.in/mgo.v2/bson"

	"gitee.com/peanut_im/global"
	"gitee.com/peanut_im/models"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

type ClientManger struct {
	clientsLock  sync.RWMutex
	clients      map[*Client]string
	singleClient map[string]*Client
}

type Client struct {
	socket  *websocket.Conn
	message ReceiveMessage
	mutex   sync.Mutex
}

type ReceiveMessage struct {
	Uuid string `json:"uuid"`
	Type string `json:"type"`
	Data string `json:"data"`
	To   string `json:"to"`
}

var manager = ClientManger{
	clients:      make(map[*Client]string, 1000),
	singleClient: make(map[string]*Client, 1000),
}

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func Pong(c *gin.Context) {
	conn, err := upgrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		fmt.Println(err)
		fmt.Println("连接失败")
		http.NotFound(c.Writer, c.Request)
		return
	}
	client := &Client{socket: conn}
	for {
		_, msg, err := client.socket.ReadMessage()
		if err != nil {
			fmt.Println("读取消息失败")
			client.socket.Close()
		}
		fmt.Printf("msg: %v", msg)
		client.socket.WriteMessage(websocket.TextMessage, []byte("pong"))
	}
}

func ConnectWeb(c *gin.Context) {
	session := sessions.Default(c)
	v := session.Get("sessionid")
	conn, err := upgrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		fmt.Println(err)
		http.NotFound(c.Writer, c.Request)
		return
	}
	client := &Client{socket: conn}
	var receiveMsg ReceiveMessage

	receiveMsg.Uuid = v.(string)
	manager.register(client, receiveMsg)

	// 心跳检测
	go func() {
		var err error
		for {
			client.mutex.Lock()
			err = client.socket.WriteMessage(websocket.PingMessage, nil)
			client.mutex.Unlock()
			if err != nil {
				fmt.Println("检测不到心跳,即将关闭连接")
				client.socket.Close()
				if uuid, ok := manager.clients[client]; ok {
					manager.DelClients(client, uuid)
				} else {
					delete(manager.clients, client)
				}
				return
			}

			time.Sleep(30 * time.Second)
		}
	}()

	go client.read()
}

func (c *Client) read() {
	var receiveMsg models.Message
	for {
		_, msg, err := c.socket.ReadMessage()
		if err != nil {
			fmt.Println("读取消息失败")
			c.socket.Close()
			if uuid, ok := manager.clients[c]; ok {
				manager.DelClients(c, uuid)
			} else {
				delete(manager.clients, c)
			}
			break
		}
		json.Unmarshal(msg, &receiveMsg)

		var user models.User
		models.FindOne(global.DB, "user", bson.M{"_id": bson.ObjectIdHex(receiveMsg.Sendid)}, nil, &user)
		receiveMsg.Name = user.Info.Name
		receiveMsg.Avatar = user.Info.Avatar
		switch receiveMsg.Type {
		case "single":
			if toClient, ok := manager.singleClient[receiveMsg.Recid]; ok {
				var msg models.MessageRes
				receiveMsg.Image = strings.Replace(receiveMsg.Image, " ", "+", -1)
				receiveMsg.Avatar = strings.Replace(receiveMsg.Avatar, " ", "+", -1)
				
				msg.Image = receiveMsg.Image
				msg.Content = receiveMsg.Content
				msg.PublishedTime = receiveMsg.PublishedTime
				msg.Sendid = receiveMsg.Sendid
				msg.Name = receiveMsg.Name
				msg.Avatar = receiveMsg.Avatar
				msg.Type = "single"
				message, _ := json.Marshal(msg)
				toClient.write(string(message))
			} else {
				fmt.Println(receiveMsg.Recid)
				receiveMsg.Mesid = bson.NewObjectId()
				models.Update(global.DB, "user", bson.M{"_id": bson.ObjectIdHex(receiveMsg.Recid)}, bson.M{"$push": bson.M{"unreadmessage": receiveMsg}})
				fmt.Println("没有发现client")
			}
		case "broadcast":
			var group models.Group
			models.FindOne(global.DB, "group", bson.M{"number": receiveMsg.Recid}, nil, &group)
			for _, v := range group.Members {
				if toClient, ok := manager.singleClient[v]; ok {
					var msg models.MessageRes
					msg.Image = receiveMsg.Image
					msg.Content = receiveMsg.Content
					msg.PublishedTime = receiveMsg.PublishedTime
					msg.Sendid = receiveMsg.Sendid
					msg.Name = receiveMsg.Name
					msg.Type = "broadcast"
					msg.Recid = receiveMsg.Recid
					message, _ := json.Marshal(msg)
					toClient.write(string(message))
				} else {
					receiveMsg.Mesid = bson.NewObjectId()
					models.Update(global.DB, "user", bson.M{"_id": bson.ObjectIdHex(v)}, bson.M{"$push": bson.M{"unreadmessage": receiveMsg}})
					fmt.Println("没有发现client")
				}
			}
		}
	}
}

func (c *Client) write(msg string) {
	err := c.socket.WriteMessage(websocket.TextMessage, []byte(msg))
	if err != nil {
		c.socket.Close()
		fmt.Println(err)
	}
	fmt.Print("发送成功")
}

func (manager *ClientManger) register(client *Client, receive ReceiveMessage) {
	_, ok := manager.clients[client]
	if !ok {
		manager.AddClients(client, receive.Uuid)
	}
}

func (manager *ClientManger) AddClients(client *Client, uuid string) {
	manager.clientsLock.Lock()
	defer manager.clientsLock.Unlock()
	manager.clients[client] = uuid
	manager.singleClient[uuid] = client
	fmt.Println("uid: ", uuid)
	fmt.Println("client: ", client)
}

func (manager *ClientManger) DelClients(client *Client, uuid string) {
	manager.clientsLock.Lock()
	defer manager.clientsLock.Unlock()
	if _, ok := manager.clients[client]; ok {
		delete(manager.clients, client)
	}
	_, ok := manager.singleClient[uuid]
	if ok {
		delete(manager.singleClient, uuid)
	}
}

func GetMessages(c *gin.Context) {
	session := sessions.Default(c)
	v := session.Get("sessionid")
	var u models.User
	models.FindOne(global.DB, "user", bson.M{"_id": bson.ObjectIdHex(v.(string))}, nil, &u)
	for _, value := range u.UnreadMessage {
		models.Update(global.DB, "user", bson.M{"_id": bson.ObjectIdHex(v.(string))}, bson.M{"$pull": bson.M{"unreadmessage": value}})
	}
	c.JSON(http.StatusOK, &Response{
		Code:    200,
		Type:    global.StatusSuccess,
		Message: u.UnreadMessage,
	})
}
