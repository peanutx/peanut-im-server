package routers

import (
	"encoding/json"
	"net/http"
	"strconv"
	"strings"
	"time"

	//"fmt"

	"gitee.com/peanut_im/global"
	"gitee.com/peanut_im/models"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2/bson"
)

func PublishMoment(c *gin.Context) {
	session := sessions.Default(c)
	v := session.Get("sessionid")
	var momentreq models.MomentsReq
	var moment models.Moment
	json.NewDecoder(c.Request.Body).Decode(&momentreq)

	moment.Mid = bson.NewObjectId()
	for _, v := range momentreq.Image {
		v = strings.Replace(v, " ", "+", -1)
		moment.Image = append(moment.Image, v)
	}
	moment.Content = momentreq.Content
	moment.PublisherID = v.(string)
	timeUnix := time.Now().Unix() //已知的时间戳
	str := strconv.FormatInt(timeUnix, 10)
	moment.PublishedTime = str
	var friends models.Friend
	models.FindOne(global.DB, "friend", bson.M{"_id": bson.ObjectIdHex(v.(string))}, nil, &friends)
	//moment.Friends = friends.Friends.Hex()
	moment.Friends = append(moment.Friends, v.(string))
	for _, value := range friends.Friends {
		moment.Friends = append(moment.Friends, value.Hex())
	}
	models.Insert(global.DB, "moment", moment)
	c.JSON(http.StatusOK, &Response{
		Code:    200,
		Type:    global.StatusSuccess,
		Message: "",
	})
}

func GetMoments(c *gin.Context) {
	session := sessions.Default(c)
	v := session.Get("sessionid")
	var moments []models.Moment
	var momentsres []models.MomentRes
	models.FindAll(global.DB, "moment", bson.M{"friends": v.(string)}, nil, &moments)

	for _, value := range moments {
		var moment models.MomentRes
		moment.Mid = value.Mid
		moment.Image = value.Image
		moment.Content = value.Content
		moment.PublisherID = value.PublisherID
		moment.PublishedTime = value.PublishedTime
		moment.Comments = value.Comments
		var likers []string
		for _, va := range value.Likes {
			var liker models.User
			models.FindOne(global.DB, "user", bson.M{"_id": bson.ObjectIdHex(va.LikerId)}, nil, &liker)
			likers = append(likers, liker.Info.Name)
		}
		var user models.User
		models.FindOne(global.DB, "user", bson.M{"_id": bson.ObjectIdHex(moment.PublisherID)}, nil, &user)
		moment.Avatar = user.Info.Avatar
		moment.PublisherName = user.Info.Name
		moment.LikeNames = likers
		momentsres = append(momentsres, moment)
	}
	//c.Find(bson.M{"tags": bson.M{"$in": []string{"a"}}}).All(&articles)
	//models.FindAll(db, "moment", bson.M{"Friends": bson.M{"$in": []string{v.(string)}}}, nil, &moments)
	c.JSON(http.StatusOK, &Response{
		Code:    200,
		Type:    global.StatusSuccess,
		Message: momentsres,
	})
}

func GetUserMoments(c *gin.Context) {
	session := sessions.Default(c)
	v := session.Get("sessionid")
	var moments []models.Moment
	var momentsres []models.MomentRes
	var friend models.Friend
	models.FindOne(global.DB, "friend", bson.M{"_id": bson.ObjectIdHex(v.(string))}, nil, &friend)
	flag := false
	for _, value := range friend.Friends {
		if value.Hex() == c.Param("fid") {
			flag = true
			break
		}
	}
	if !flag {
		if c.Param("fid") != v.(string) {
			c.JSON(http.StatusOK, &Response{
				Code:    400,
				Type:    global.StatusFail,
				Message: "the user is not your friend",
			})
			return
		}
	}

	models.FindAll(global.DB, "moment", bson.M{"publisherId": c.Param("fid")}, nil, &moments)

	for _, value := range moments {
		var moment models.MomentRes
		moment.Mid = value.Mid
		moment.Image = value.Image
		moment.Content = value.Content
		moment.PublisherID = value.PublisherID
		moment.PublishedTime = value.PublishedTime
		moment.Comments = value.Comments
		for _, vb := range moment.Comments {
			var liker models.User
			models.FindOne(global.DB, "user", bson.M{"_id": bson.ObjectIdHex(vb.PublisherId)}, nil, &liker)
			vb.PublisherName = liker.Info.Name

		}
		var likers []string
		for _, va := range value.Likes {
			var liker models.User
			models.FindOne(global.DB, "user", bson.M{"_id": bson.ObjectIdHex(va.LikerId)}, nil, &liker)
			likers = append(likers, liker.Info.Name)
		}
		moment.LikeNames = likers
		var user models.User
		models.FindOne(global.DB, "user", bson.M{"_id": bson.ObjectIdHex(moment.PublisherID)}, nil, &user)
		moment.Avatar = user.Info.Avatar
		moment.PublisherName = user.Info.Name
		momentsres = append(momentsres, moment)
	}
	c.JSON(http.StatusOK, &Response{
		Code:    200,
		Type:    global.StatusSuccess,
		Message: momentsres,
	})
}

//发表一条评论
func AddComment(c *gin.Context) {
	session := sessions.Default(c)
	v := session.Get("sessionid")

	var commentreq models.CommentReq
	var comment models.Comment
	json.NewDecoder(c.Request.Body).Decode(&commentreq)

	var moment models.Moment

	models.FindOne(global.DB, "moment", bson.M{"_id": bson.ObjectIdHex(commentreq.MomentId)}, nil, &moment)
	if moment.Mid == "" {
		c.JSON(http.StatusOK, &Response{
			Code:    400,
			Type:    global.StatusFail,
			Message: "moment not exist",
		})
		return
	}
	comment.Cid = bson.NewObjectId()
	comment.Content = commentreq.Content
	comment.PublisherId = v.(string)
	var commenter models.User
	models.FindOne(global.DB, "user", bson.M{"_id": bson.ObjectIdHex(v.(string))}, nil, &commenter)
	comment.PublisherName = commenter.Info.Name
	timeUnix := time.Now().Unix() //已知的时间戳
	comment.PublishedTime = strconv.FormatInt(timeUnix, 10)
	models.Update(global.DB, "moment", bson.M{"_id": bson.ObjectIdHex(commentreq.MomentId)}, bson.M{"$push": bson.M{"comments": comment}})
	c.JSON(http.StatusOK, &Response{
		Code:    200,
		Type:    global.StatusSuccess,
		Message: "",
	})
}

func AddLike(c *gin.Context) {
	session := sessions.Default(c)
	v := session.Get("sessionid")

	var likereq models.LikeReq
	var like models.Like
	json.NewDecoder(c.Request.Body).Decode(&likereq)

	var moment models.Moment

	models.FindOne(global.DB, "moment", bson.M{"_id": bson.ObjectIdHex(likereq.MomentId)}, nil, &moment)
	if moment.Mid == "" {
		c.JSON(http.StatusOK, &Response{
			Code:    400,
			Type:    global.StatusFail,
			Message: "moment not exist",
		})
		return
	}

	like.Lid = bson.NewObjectId()
	like.LikerId = v.(string)
	for _, value := range moment.Likes {
		if value.LikerId == v.(string) {
			c.JSON(http.StatusOK, &Response{
				Code:    400,
				Type:    global.StatusFail,
				Message: "like exist",
			})
			return
		}
	}
	models.Update(global.DB, "moment", bson.M{"_id": bson.ObjectIdHex(likereq.MomentId)}, bson.M{"$push": bson.M{"likes": like}})

	c.JSON(http.StatusOK, &Response{
		Code:    200,
		Type:    global.StatusSuccess,
		Message: "",
	})
}

/*func AddReply(c *gin.Context) {
	session := sessions.Default(c)
	v := session.Get("sessionid")

	var replyreq models.ReplyReq
	var reply models.Reply
	json.NewDecoder(c.Request.Body).Decode(&replyreq)

	var comment models.Comment
	models.FindOne(db, "moment", bson.M{"comment": replyreq.CommentId}, nil, &comment)
	if comment == nil {
		c.JSON(http.StatusOK, &Response {
			Code: 400,
			Type: "fail",
			Message:  "comment does not exist",
		})
		return
	}
	reply.Content = replyreq.Content
	reply.PublisherId = v.(string)
	models.Update(db, "moment", bson.M{"_id": bson.ObjectIdHex(commentreq.MomentId)}, bson.M{"$push": bson.M{"comments": comment}})
	c.JSON(http.StatusOK, &Response {
		Code: 200,
		Type: "success",
		Message:  "",
	})
}*/

//删除一条评论
func DeleteCommentByCid(c *gin.Context) {

}
