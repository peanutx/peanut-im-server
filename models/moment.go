package models

import (
	"gopkg.in/mgo.v2/bson"
)

type Moment struct {
	Mid           bson.ObjectId `bson:"_id" json:"id"`
	Image         []string      `bson:"image" json:"image"`                 //图片
	Content       string        `bson:"content" json:"content"`             //文字内容
	PublisherID   string        `bson:"publisherId" json:"publisherid"`     //发布者id
	PublishedTime string        `bson:"publishedTime" json:"publishedtime"` //发布时间
	Friends       []string      `bson:"friends" json:"friends"`             //朋友可见
	Comments      []Comment     `bson:"comments" json:"comments"`           //评论
	Likes         []Like        `bson:"likes" json:"likes"`                 //点赞
}

type Reply struct {
	Rid         bson.ObjectId `bson:"_id" json:"id"`
	Content     string        `bson:"content" json:"content"`         //评论内容
	PublisherId string        `bson:"publisherId" json:"publisherid"` //评论者id
}

type Comment struct {
	Cid           bson.ObjectId `bson:"_id" json:"id"`
	Content       string        `bson:"content" json:"content"`             //评论内容
	PublisherId   string        `bson:"publisherId" json:"publisherid"`     //评论者id
	PublisherName string        `bson:"publisherName" json:"publishername"` //评论者name
	PublishedTime string        `bson:"publishedTime" json:"publishedtime"`
	//Replys []Reply `bson:"replys"`
}

type Like struct {
	Lid     bson.ObjectId `bson:"_id" json:"id"`
	LikerId string        `bson:"likerid" json:"likerid"` //点赞者id
}

type MomentsReq struct {
	Image   []string `bson:"image" json:"image"`
	Content string   `bson:"content" json:"content"`
}

type MomentRes struct {
	Mid           bson.ObjectId `bson:"_id" json:"id"`
	Image         []string      `bson:"image" json:"image"`
	Content       string        `bson:"content" json:"content"`
	PublisherID   string        `bson:"publisherId" json:"publisherid"`
	Avatar        string        `bson:"avatar" json:"avatar"` // 头像URL
	PublishedTime string        `bson:"publishedTime" json:"publishedtime"`
	Comments      []Comment     `bson:"comments" json:"comments"`
	LikeNames     []string      `bson:"likeNames" json:"likenames"`
	PublisherName string        `bson:"publisherName" json:"publishername"`
	//Likes         []Like        `bson:"likes"`
}

type CommentReq struct {
	MomentId string `bson:"momentid" json:"momentid"`
	Content  string `bson:"content" json:"content"` //评论内容
	//PublishedTime string `bson:"publishedTime"`
}

type LikeReq struct {
	MomentId string `bson:"momentid" json:"momentid"`
}

type ReplyReq struct {
	CommentId string `bson:"commentid" json:"momentid"`
	Content   string `bson:"content" json:"content"` //评论内容
}
