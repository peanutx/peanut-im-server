package models

import (
	"errors"

	"gitee.com/peanut_im/utils"
	"gopkg.in/mgo.v2/bson"
)

type User struct {
	ID            bson.ObjectId `bson:"_id" json:"id"`                      // 用户IDs
	VioletID      bson.ObjectId `bson:"vid" json:"vid"`                     // VioletID
	Token         string        `bson:"token" json:"token"`                 // Violet 访问令牌
	Email         string        `bson:"email" json:"email"`                 // 用户唯一邮箱
	Password      string        `bson:"password" json:"password"`           // 密码
	Info          UserInfo      `bson:"info" json:"info"`                   // 用户个性信息
	UnreadMessage []Message     `bson:"unreadmessage" json:"unreadmessage"` //用户未读消息
	Group         []string      `bson:"group" json:"group"`                 //用户加入的群聊
}

// UserInfo 用户个性信息
type UserInfo struct {
	Name   string `bson:"name" json:"name"`     // 昵称
	Avatar string `bson:"avatar" json:"avatar"` // 头像URL
	Bio    string `bson:"bio" json:"bio"`       // 个人简介
	Gender int    `bson:"gender" json:"gender"` // 性别
}

type FriendInfo struct {
	Email  string `bson:"email" json:"email"`   // 用户唯一邮箱
	Name   string `bson:"name" json:"name"`     // 昵称
	Avatar string `bson:"avatar" json:"avatar"` // 头像URL
	Bio    string `bson:"bio" json:"bio"`       // 个人简介
	Gender int    `bson:"gender" json:"gender"` // 性别
}

type RegisterReq struct {
	Name     string `json:"name"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

// RegisterReq POST /login login请求
type LoginReq struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type ChangeNameReq struct {
	Name string `json:"name"`
}

type ByID struct {
	ID string `json:"_id"`
}

type JwtToken struct {
	Token string `json:"token"`
}

const (
	db         = "peanut_im"
	collection = "user"
)

func Register(name, email, password string) error {
	var users []User
	FindAll(db, collection, bson.M{"email": email}, nil, &users)
	if len(users) != 0 {
		return errors.New("email has exits")
	}

	return AddUser(email, password, UserInfo{
		Name:   name,
		Avatar: "",
		Bio:    "",
		Gender: 0,
	})
}

func AddUser(email, password string, userinfo UserInfo) error {
	newUser := bson.NewObjectId()
	var data []byte = []byte(password)
	hashCode := utils.GetSHA256HashCode(data)
	user := User{
		ID:       newUser,
		VioletID: newUser,
		Token:    "",
		Email:    email,
		Password: hashCode,
		Info:     userinfo,
	}
	userfriend := Friend{
		ID:      newUser,
		Friends: nil,
	}
	Insert(db, collection, user)
	Insert(db, "friend", userfriend)
	return nil
}

func Login(name, password string) error {
	var users []User
	FindAll(db, collection, bson.M{"email": name}, nil, &users)
	if len(users) == 0 {
		return errors.New("user not exits")
	}
	var data []byte = []byte(password)
	hashCode := utils.GetSHA256HashCode(data)
	if hashCode != users[0].Password {
		return errors.New("password error")
	}
	return nil
}

/*func FindAllMovies() ([]Movies, error) {
	var result []Movies
	err := FindAll(db, collection, nil, nil, &result)
	return result, err
}

func FindMovieById(id string) (Movies, error) {
	var result Movies
	err := FindOne(db, collection, bson.M{"_id": bson.ObjectIdHex(id)}, nil, &result)
	return result, err
}

func UpdateMovie(movie Movies) error {
	return Update(db, collection, bson.M{"_id": movie.Id}, movie)
}

func RemoveMovie(id string) error {
	return Remove(db, collection, bson.M{"_id": bson.ObjectIdHex(id)})
}*/
