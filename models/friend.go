package models

import (
	"gopkg.in/mgo.v2/bson"
)

type Friend struct {
	ID bson.ObjectId `bson:"_id" json:"id"`          // 用户IDs
	Friends []bson.ObjectId `bson:"fid" json:"friends"`   
}

type AddFriendReq struct {
	Email string `bson:"email" json:"email"`          // 用户IDs 
}
