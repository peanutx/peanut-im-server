package models

import (
	"gopkg.in/mgo.v2/bson"
)

type Group struct {
	Gid          bson.ObjectId `bson:"_id" json:"id"`
	Number       string        `bson:"number" json:"number"`       //群号
	Name         string        `bson:"name" json:"name"`         //群名
	Ownerid      string        `bson:"ownerid" json:"ownerid"`      //群主Id
	Members      []string      `bson:"members" json:"members"`      //群成员
	Membersname  []string      `bson:"membersname" json:"membersname"`  //群成员的姓名
	Announcement string        `bson:"announcement" json:"announcement"` //群公告
}

type GroupReq struct {
	Name string `bson:"name" json:"name"`
}

type AnnouncementReq struct {
	Number       string `bson:"number" json:"number"`
	Announcement string `bson:"announcement" json:"announcement"`
}

type DeleteReq struct {
	Number   string `bson:"number" json:"number"`
	MemberId string `bson:"memberid" json:"memberid"`
}

//加群，删群，踢出群聊，创建群聊
