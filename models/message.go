package models

import (
	"gopkg.in/mgo.v2/bson"
)

type Message struct {
	Mesid         bson.ObjectId `bson:"_id" json:"id"`
	Type          string        `bson:"type" json:"type"`
	Sendid        string        `bson:"sendid" json:"sendid"`
	Name          string        `bson:"name" json:"name"`
	Recid         string        `bson:"recid" json:"recid"` //接收者的id是用户的id或群的id
	Image         string        `bson:"image" json:"image"`
	Content       string        `bson:"content" json:"content"`
	PublishedTime string        `bson:"publishedTime" json:"publishedtime"`
	Avatar 		  string		`bson:"avatar" json:"avatar"`
}

type MessageRes struct {
	Sendid        string `bson:"sendid" json:"sendid"`
	Type          string `bson:"type" json:"type"`
	Image         string `bson:"image" json:"image"`
	Content       string `bson:"content" json:"content"`
	PublishedTime string `bson:"publishedTime" json:"publishedtime"`
	Name          string `bson:"name" json:"name"`
	Recid         string `bson:"recid" json:"recid"`  //接收者的id是用户的id或群的id
	Avatar        string `bson:"avatar" json:"avatar"` // 头像URL
}
